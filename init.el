;;===========================================================================
;; Murhytalk's emacs config
;; $Id: .emacs 8 2007-06-14 00:28:06Z lumu75 $
;;===========================================================================

;;===========================================================================
;;{{{ Basic settings

;;===========================================================================
;;-----------------------------------------
;;change application title format
;;------------------------------------------
(setq frame-title-format "%b@emacs")

;;-------------------------------------------
;;default path while C-x C-f
;;-------------------------------------------
(setq-default default-directory "~")

;;----------------------------------------------------------------------
;;I put lisp codes written or modified by me here
;;----------------------------------------------------------------------
(add-to-list 'load-path "~/.emacs.d/local-lisp")

;;-------------------------------------------
;; save emacs custom craps in another file
;;-------------------------------------------
(setq custom-file "~/.emacs.d/.my-emacs-custom.el")
(load custom-file)

;;-------------------------------------------
;; save backups in ~/.emacs.d/backups folder
;;-------------------------------------------
(setq backup-directory-alist (quote (("." . "~/.emacs.d/backups"))))
(setq kept-new-versions 5)
(setq delete-old-versions t)
(setq backup-by-copying t)
;;}}}

;;auto refresh changed buffers
(global-auto-revert-mode t)

;; Packages
(setq package-archives '(("ELPA" . "http://tromey.com/elpa/") 
                         ("gnu" . "http://elpa.gnu.org/packages/")))

;; Add the user-contributed repository
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))


;;===========================================================================
;;{{{ Font/UI/layout

;;===========================================================================
(set-fontset-font "fontset-default"
'gb18030 '("Microsoft YaHei" . "unicode-bmp"))


;;-------------------------------------------
;; color theme
;;-------------------------------------------
(load-theme 'tango-dark)

;;-------------------------------------------
;; font
(set-default-font "Consolas-10")
;;-------------------------------------------

;; display column number
(column-number-mode 1)
;; add these lines if you like color-based syntax highlighting
(global-font-lock-mode t)

;;---------------------------------------------------
;; highlight the matching parens
;;---------------------------------------------------
(show-paren-mode t)
(setq show-paren-style 'parentheses)

;;----------------------------------------------------------------------------
;  highlighting typedef
;;----------------------------------------------------------------------------
(require 'ctypes)
(ctypes-auto-parse-mode 1)

(require 'smooth-scrolling)

;;----------------------------------------------------------------------------
; layout
;;----------------------------------------------------------------------------

;;turn off toolbar
(tool-bar-mode 0)

;;turn off menubar
(menu-bar-mode 0)

;;move scroll bar to right side of the frame
;(set-scroll-bar-mode 'right)
;;turn off scroll bar
(set-scroll-bar-mode 'nil)

;;use visible bell,be quiet
(setq visible-bell t)

;;}}}

;;===========================================================================
;;{{{ Basic actions/features

;;===========================================================================

;;-------------------------------------------
; if the cursor is at the head of a line then
; C-k would delete the whole line
;;-------------------------------------------
(setq-default kill-whole-line t)

;;-------------------------------------------
; a big kill ring
;;-------------------------------------------
(setq kill-ring-max 200)

;;-------------------------------------------
; keep mouse be away from the cursor
;;-------------------------------------------
(mouse-avoidance-mode 'animate)

;;-------------------------------------------
; if file is an image then show it
;;-------------------------------------------
(auto-image-file-mode)

;;-------------------------------------------
;;disable emacs startup message/logo
;;-------------------------------------------
(setq inhibit-startup-message t)

;;-------------------------------------------
;;automatically append a new line char to the end
;;of file if there is not one
;;-------------------------------------------
(setq require-final-newline t)
;;-------------------------------------------
; time format
;;-------------------------------------------
(display-time-mode 1)

(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(setq display-time-use-mail-icon t)
(setq display-time-interval 10)

;;show time on mode line
(display-time)

;;answer emacs with y/n,instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)

;do not show warning message while open large file
(setq large-file-warning-threshold nil)

;delete directory recusively while in dired mode
(setq dired-recursive-deletes 'top)

;delete region without saving to killing ring
(global-set-key [(meta delete)] 'delete-region)

;;enable the region down/up case
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;;-----------------------------------------------------------
;; switch buffer by using sub-string
;;{{{ iswitchb default key-bindings
;; C-s/C-r : Move in the candicates list
;; C-k     : Kill the buffer at the head of the list
;; C-t     : Toggle regexp searching.
;; C-c     : Toggle case-sensitive searching of buffer names.
;;}}}
;;------------------------------------------------------------
(iswitchb-mode 1)

;;}}}

;;===========================================================================
;;{{{ Printing
;;===========================================================================
;(setq lpr-command "notepad.exe")
;(setq lpr-switches '("/p"))
;(setq printer-name nil)
;(setq lpr-headers-switches nil)
;(define-process-argument-editing "/notepad\\.exe$"
;  (lambda (x) (general-process-argument-editing-function x nil t)))
;(defadvice direct-print-region-function
;  (around direct-print-region-function-around activate)
;  (let ((coding-system-for-write 'japanese-shift-jis-dos))
;    ad-do-it))

;;highlight mark region
;(transient-mark-mode)
;;}}}

;;===========================================================================
;;{{{ My functions

;;===========================================================================
;;---------------------------------------------------------------------------
;; copy whole word into kill ring
;; 2007/6/20
;;---------------------------------------------------------------------------
(defun murphytalk-copy-whole-word ()
  (interactive)
  (let* ((set "^][ \t\n(){};'\",:-.")
         pos
	 beg
	 end)
    ;save current position
    (setq pos (point))
    ;save start pos of current word to beg
    (skip-chars-backward set) (setq beg (point))
    ;save end pos of current word to end
    (skip-chars-forward  set) (setq end (point))
    ;copy to kill ring
    (copy-region-as-kill beg end)
    ;move cursor back to orignial position
    (goto-char pos)
    )
)

;;}}}

;;===========================================================================
;;{{{ Programming mode
;;===========================================================================
;TABs are evil!
(setq-default indent-tabs-mode nil)

(require 'quack)

;;-------------------------------------------
;; python mode
;;-------------------------------------------
(autoload 'python-mode "python-mode.el" "Python mode." t)
(setq auto-mode-alist (append '(("/*.\.py$" . python-mode)) auto-mode-alist))
(defun my-python-mode-hook ()
  (setq tab-width 4)) ;; change this to taste, this is what K&R uses :)
(add-hook 'python-mode-hook 'my-python-mode-hook)

(setq font-lock-maximum-decoration t)
(setq auto-mode-alist
      (cons '("\\.py$" . python-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.pyw$" . python-mode) auto-mode-alist))
(setq interpreter-mode-alist
      (cons '("python" . python-mode)
            interpreter-mode-alist))
(autoload 'python-mode "python-mode" "Python editing mode." t)
;(setq ipython-command "C:/Python24/python.exe C:/Python24/scripts/ipython")
(require 'ipython)

;;-------------------------------------------
;; octave mode
;;-------------------------------------------
(autoload 'octave-mode "octave-mod" nil t)
         (setq auto-mode-alist
               (cons '("\\.m$" . octave-mode) auto-mode-alist))

;;-------------------------------------------
;; MarkDown mode
;;-------------------------------------------
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))


;;-------------------------------------------
;; CMake mode
;;-------------------------------------------
(autoload 'cmake-mode "cmake-mode" nil t)
(setq auto-mode-alist
	  (append
	   '(("CMakeLists\\.txt\\'" . cmake-mode))
	   '(("\\.cmake\\'" . cmake-mode))
	   auto-mode-alist))

;;-------------------------------------------
;; lua mode
;;-------------------------------------------
(autoload 'lua-mode "lua-mode"
  "Mode for editing lua source files")

(setq auto-mode-alist
      (append '(("\\.lua$" . lua-mode)) auto-mode-alist))
(setq interpreter-mode-alist (append '(("lua" . lua-mode))
    				     interpreter-mode-alist))

;;--------------------------------------------
;; .as(ActionScript) files ->  javascript mode
;;--------------------------------------------
;(setq auto-mode-alist
;      (append '(("\\.as\\'" . javascript-generic-mode)) auto-mode-alist))

;;-------------------------------------------
;; cc mode - TAB problem
;;-------------------------------------------
(require 'cc-mode)
(defun my-build-tab-stop-list (width)
  (let ((num-tab-stops (/ 80 width))
	(counter 1)
	(ls nil))
    (while (<= counter num-tab-stops)
      (setq ls (cons (* width counter) ls))
      (setq counter (1+ counter)))
    (set (make-local-variable 'tab-stop-list) (nreverse ls))))

(defun my-c-mode-common-hook ()
  (setq tab-width 4) ;; change this to taste, this is what K&R uses :)
  ;uncomment the following line to use hard TAB,otherwise use spaces
  ;(my-build-tab-stop-list tab-width)
  (setq c-basic-offset tab-width))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;;default styles
(setq c-default-style '((java-mode . "java")
			(awk-mode . "awk")
                        (c-mode . "k&r")
			(other . "linux")))

;;mmm mode
(add-to-list 'load-path "~/.emacs.d/local-lisp/mmm-mode-0.4.8")
;;mako template mode
(load "~/.emacs.d/local-lisp/mmm-mako.el")
(add-to-list 'auto-mode-alist '("\\.mako\\'" . html-mode))
(mmm-add-mode-ext-class 'html-mode "\\.mako\\'" 'mako)

;;}}}

;;===========================================================================
;;{{{ Make my programming life be easier ;)

;;===========================================================================
;;--------------------------------------------------------------------------------------
; folding mode
;;--------------------------------------------------------------------------------------
(autoload 'folding-mode          "folding" "Folding mode" t)
(autoload 'turn-off-folding-mode "folding" "Folding mode" t)
(autoload 'turn-on-folding-mode  "folding" "Folding mode" t)
(folding-mode);turn folding mode,otherwise the following will cause runtime error
(folding-add-to-marks-list 'c-mode' "// {{{ " "// }}}")
(folding-add-to-marks-list 'python-mode' "## {{{ " "## }}}")
(folding-mode)

;;--------------------------------------------------------------------------------------
; imenu  mode
; imenu popup menu is binded to Shift-RightMouseButton
;;--------------------------------------------------------------------------------------
(setq imenu-sort-function 'imenu--sort-by-name) ;sort functions by alphabet
;bind imenu popup menu to Shift-RightMouseButton
(global-set-key [S-mouse-3] 'imenu)


;;--------------------------------------------------------------------------------------
;; Go to the matching paren if on a paren; otherwise insert %.
;;--------------------------------------------------------------------------------------
(defun match-paren (arg)
  "Go to the matching paren if on a paren; otherwise insert %."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
	((looking-at "\\s\)") (forward-char 1) (backward-list 1))
	(t (self-insert-command (or arg 1)))))

;;and bind it to Ctrl-% key
;(global-set-key "%" 'match-paren)
(global-set-key [(control ?\% )] 'match-paren)

;;--------------------------------------------------------------------------------------
;; Temp bookmark
;; C-. to make a temp bookmark, and C-, to jump back
;;--------------------------------------------------------------------------------------
(global-set-key [(control ?\.)] 'ska-point-to-register)
(global-set-key [(control ?\,)] 'ska-jump-to-register)
(defun ska-point-to-register()
  "Store cursorposition _fast_ in a register.Use ska-jump-to-register to jump back to the stored position."
  (interactive)
  (setq zmacs-region-stays t)
  (point-to-register 8))

(defun ska-jump-to-register()
  "Switches between current cursorposition and position that was stored with ska-point-to-register."
  (interactive)
  (setq zmacs-region-stays t)
  (let ((tmp (point-marker)))
        (jump-to-register 8)
        (set-register 8 tmp)))

;;}}}

;;===========================================================================
;;{{{ Some other auto modes (file types)

;;===========================================================================
(require 'generic-x)
;fvwm config files
(setq auto-mode-alist
      (cons '("\\.fvwm.*/" . fvwm-generic-mode) auto-mode-alist))

;NSIS files
(autoload 'nsi-mode "nsi-mode" "nsi editing mode." t)
(add-to-list 'auto-mode-alist '("\\.nsi$" . nsi-mode))

;; txt2tags mode
(setq auto-mode-alist (append (list
	'("\\.t2t$" . t2t-mode)
	)
	(if (boundp 'auto-mode-alist) auto-mode-alist)
))
(autoload  't2t-mode "txt2tags-mode" "Txt2tags Mode" t)

;;-------------------------------------------
;; planner mode
;;-------------------------------------------
;(add-to-list 'load-path "~/.emacs.d/local-lisp/emacs-wiki/")
;(add-to-list 'load-path "~/.emacs.d/local-lisp/planner/")
;(require 'planner)

;(global-set-key [f9] 'planner-create-task-from-buffer)

;;-----------------------------------------------------
;; org mode
;;-----------------------------------------------------
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)

;;
(autoload 'screen-lines-mode "screen-lines"
          "Toggle Screen Lines minor mode for the current buffer." t)
(autoload 'turn-on-screen-lines-mode "screen-lines"
          "Turn on Screen Lines minor mode for the current buffer." t)
(autoload 'turn-off-screen-lines-mode "screen-lines"
          "Turn off Screen Lines minor mode for the current buffer." t)

;; comment out the following lines if you want the original `kill-line'
;; in Screen Lines minor mode.
;;(add-hook 'screen-lines-load-hook
;;          #'(lambda ()
;;               (ad-disable-advice 'kill-line 'around 'screen-lines)
;;               (ad-activate 'kill-line)))


(require 'moinmoin-mode)
(require 'rst)
;;}}}

;;===========================================================================
;;{{{ PIM related
;;===========================================================================
(add-hook 'diary-hook 'appt-make-list)
; some file/folder deafults
(setq diary-file "~/.emacs.d/PIM/diary.txt")
(setq todo-file-do "~/.emacs.d/emacs/todo/do")
(setq todo-file-done "~/.emacs.d/emacs/todo/done")
(setq todo-file-top "~/.emacs.d/emacs/todo/top")
(setq diary-file "~/.emacs.d/emacs/diary")
(setq diary-file "~/.emacs.d/PIM/diary.txt")
;;}}}

;;===========================================================================
;;{{{ 3rd party packages/tools

;;===========================================================================

;;-----------------------------------------------------------------------
;; cscope
;;-----------------------------------------------------------------------
(require 'xcscope)
(add-hook 'java-mode-hook (function cscope:hook))
(add-hook 'python-mode-hook (function cscope:hook))

;;-----------------------------------------------------------------------
;; tramp config on Windows : by using pscp
;;-----------------------------------------------------------------------
(defun set_tramp_on_win()
  (modify-coding-system-alist 'process "plink" 'utf-8-unix)
  (setq tramp-default-method "plink"
	tramp-completion-without-shell-p t)
  (require 'tramp)
)

(if (string-match "nt" system-configuration)
    (set_tramp_on_win))

;;-------------------------------------------
;; ibuffer mode
;;-------------------------------------------
(require 'ibuffer)

;
(require 'server)
(when (and (= emacs-major-version 23) (equal window-system 'w32))
  (defun server-ensure-safe-dir (dir) "Noop" t)) ; Suppress error "directory
                                                 ; ~/.emacs.d/.emacs.d/server is unsafe"
                                                 ; on windows.
(server-start)

;;-------------------------------------------
;; doxygen
;;-------------------------------------------
;;{{{ Default key bindings are:
;;   - C-c d ? will look up documentation for the symbol under the point.
;;   - C-c d r will rescan your Doxygen tags file.
;;   - C-c d f will insert a Doxygen comment for the next function.
;;   - C-c d i will insert a Doxygen comment for the current file.
;;   - C-c d ; will insert a Doxygen comment for the current member.
;;   - C-c d m will insert a blank multiline Doxygen comment.
;;   - C-c d s will insert a blank singleline Doxygen comment.
;;   - C-c d @ will insert grouping comments around the current region.
;;}}}
;;-------------------------------------------
(require 'doxymacs)
(add-hook 'python-mode-hook 'doxymacs-mode)
;;turn on doxymacs-mode with c/c++ files
(add-hook 'c-mode-common-hook'doxymacs-mode)
;;turn on doxygen keywords font lock for c/c++ files
(defun my-doxymacs-font-lock-hook ()
  (if (or (eq major-mode 'c-mode) (eq major-mode 'c++-mode))
      (doxymacs-font-lock)))
(add-hook 'font-lock-mode-hook 'my-doxymacs-font-lock-hook)

;-------------------------------------------
; session
;-------------------------------------------
(require 'session)
(add-hook 'after-init-hook 'session-initialize)
(setq desktop-globals-to-save '(desktop-missing-file-warning))

;-------------------------------------------
; desktop
;-------------------------------------------
(desktop-save-mode 1)
(setq desktop-buffers-not-to-save
        (concat "\\(" "^nn\\.a[0-9]+\\|\\.log\\|(ftp)\\|^tags\\|^TAGS"
	        "\\|\\.emacs.*\\|\\.diary\\|\\.newsrc-dribble\\|\\.bbdb"
	        "\\)$"))
;--------------------------------------------
;browse killing ring in a buffer
;C-h m OR ? in its buffer to get help
;---------------------------------------------
(require 'browse-kill-ring)
(global-set-key [(control c)(k)] 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;;--------------------------------------------------------------------------------------
; Dired enhancements
;;--------------------------------------------------------------------------------------
(require 'dired+)

(require 'dired-single)

(defun my-dired-init ()
  "Bunch of stuff to run for dired, either immediately or when it's
   loaded."
  ;; <add other stuff here>
  (define-key dired-mode-map [return] 'joc-dired-single-buffer)
  (define-key dired-mode-map [mouse-1] 'joc-dired-single-buffer-mouse)
  (define-key dired-mode-map "^"
        (function
         (lambda nil (interactive) (joc-dired-single-buffer "..")))))

;; if dired's already loaded, then the keymap will be bound
(if (boundp 'dired-mode-map)
        ;; we're good to go; just add our bindings
        (my-dired-init)
  ;; it's not loaded yet, so add our bindings to the load-hook
  (add-hook 'dired-load-hook 'my-dired-init))

;;--------------------------------------------------------------------------------------
; C-x w to launch w3m
;;--------------------------------------------------------------------------------------
;(defun def_w3m_hotkey()
;  (add-to-list 'load-path "~/.emacs.d/local-lisp/emacs-w3m")
;  (require 'w3m)
;(global-set-key [(control x) (w)] '(lambda ()
;				   (interactive)
;				   (w3m)
;;				   (setq w3m-command-arguments
;;					 (nconc w3m-command-arguments
;;						'("-o" "http_proxy=http://proxy2:8080/")))
;				   ))
;)
;(def_w3m_hotkey)
;(if(equal (system-name) "PC077157") (def_w3m_hotkey)())
;def_w3m_hotkey
;;----------------------------------------------------------------------
;;save as buffer as HTML whith syntax highlighting
;;----------------------------------------------------------------------
;(load "htmlize")
;(require 'generic-x)

;;}}}



;;===========================================================================
;;{{{ TinyTools collection

;;===========================================================================
(add-to-list 'load-path "~/.emacs.d/local-lisp/emacs-tiny-tools/lisp/tiny")
(add-to-list 'load-path "~/.emacs.d/local-lisp/emacs-tiny-tools/lisp/other")
;(load-library "tiny-setup")

;;--------------------------------------------------------------------------
;;tiny advice
;;--------------------------------------------------------------------------
;(require 'tinyadvice)
;(require 'tinylib)
;(when (emacs-p)                           ;Do not load in XEmacs
;  (if (fboundp 'run-with-idle-timer)      ;Emacs
;      (run-with-idle-time (* 4 60) nil '(lambda () (require 'tinyadvice)))
;    (run-at-time "4 min" nil '(lambda () (require 'tinyadvice)))))
;(setq tinyadvice-:file-compress-support t)

;;--------------------------------------------------------------------------
;;tiny cache
;;--------------------------------------------------------------------------
;(eval-after-load "dired"   '(progn (require 'tinycache)))

;;--------------------------------------------------------------------------
;;tiny comment
;;--------------------------------------------------------------------------
(global-set-key  "\M-;" 'tinycomment-indent-for-comment)
(autoload 'tinycomment-indent-for-comment "tinycomment" "" t)

;;--------------------------------------------------------------------------
;;tiny eat
;;{{{ Tiney eat default key bindings
;;    chunk delete: words, spaces, symbols ...
;;
;;          <<              >>                  <<>> [Delete whole word]
;;          Alt-Backspace   Control-backspace   Shift-Backspace
;;
;;    Line delete
;;
;;          <<              >>                  <<>> [zap whole line]
;;          Alt-Backspace   Control-k (Alt-k)   Control-k
;;          + Shift                             + Alt
;;
;;    Buffer delete
;;
;;          \/              /\                  \//\             ZAP
;;          untill pmax     untill pmin         Paragraph delete Whole buffer
;;          C-A-backspace   C-A-Backspace       C-S-backspace    Esc-Backspace
;;                          + Shift
;;
;;    Joining next line to the end of current line: Esc Control-backspace
;;
;;    [Some minibuffer hotkeys]
;;
;;          f1  = Kill whole line.
;;          f2  = Delete line backward (to the left)
;;
;;    [Mouse binding]
;;
;;          (Alt|meta)-Mouse-2 overwries text when pasting.
;;}}}
;;--------------------------------------------------------------------------
;(require 'tinyeat)

;;--------------------------------------------------------------------------
;;tiny (E)lectric (f)ile minor mode
;;{{{ key bindings
;;	o   b>> means what's on the line *before*
;;	o   a>> means what's there *after*
;;	o   ""  means what you just pressed
;;	o   []  means which action the character triggered
;;
;;	    b>> http:/www.site.com/~userFoo/dir1/dir2/dir3/ "/" [e-slash]
;;	    a>> http:/
;;	    The e-slash action wiped out the line, because writing
;;	    two slashes normally indicates, that you want to give
;;	    another path
;;
;;	    b>> ~/.emacs.d/dir1/dir2/dir3/			"~" [e-tilde]
;;	    a>> ~
;;	    The action wiped the line away, because it assumed
;;	    you want to give "~userFoo" or another "~" relative path
;;
;;	    b>> ~/.emacs.d/dir1/dir2/dir3/			"[" [step-delete-back]
;;	    a>> ~/.emacs.d/dir1/dir2/
;;	    The action wiped previous directory name or until
;;	    special mark, See code, defaults are  ":/@" (ange-ftp things)
;;
;;	    b>> ~/.emacs.d/dir1/dir2/			        "=" [undo]
;;	    a>> ~/.emacs.d/dir1/dir2/dir3/
;;	    The action works like normal undo.
;;
;;	    b>> ~/.emacs.d/dir1/dir2/				"`" [chunk-delete]
;;	    a>>
;;	    The action deleted whole line. It deletes until special marks
;;	    like "@:". If repeated, it deletes constantly backward
;;}}}
;;--------------------------------------------------------------------------
;(ti::when-package 'tinyef nil
;  (autoload 'turn-on-tinyef-mode "tinyef" "" t)
;  (add-hook 'minibuffer-setup-hook 'turn-on-tinyef-mode))
;  (autoload 'turn-on-tinyef-mode "tinyef" "" t)
;  (add-hook 'minibuffer-setup-hook 'turn-on-tinyef-mode)


;;--------------------------------------------------------------------------
;;tiney my
;;--------------------------------------------------------------------------
;; do not display DOS return key
;(add-hook  'find-file-hooks  'tinymy-find-file-hook)
;; key bindings
;(setq tinymy-:load-hook '(tinymy-install tinymy-define-keys tinymy-alias))
;(require 'tinymy)

;;}}}

;;===========================================================================
;;{{{ muse mode
;;===========================================================================
;;(add-to-list 'load-path "~/.emacs.d/local-lisp/muse")
;(require 'muse-mode)
;(require 'muse-html)
;(require 'muse-project)
;;use diffrent colors instead of different text sizesto distinguish levels
;(setq muse-colors-autogen-headings 'outline)
;
;;open org minor mode for muse file
;(add-hook 'muse-mode-hook
;          '(lambda ()
;             (setq outline-regexp "\\*+ ") ;there should be a space after the section indicator(*)
;             (outline-minor-mode)
;             (hide-body)))
;;use TAB to fold/unfold sections
;(define-key muse-mode-map [tab] 'org-cycle)
;
;;style sheet used while pulish to html
;(setq muse-html-style-sheet "<link rel=\"stylesheet\" type=\"text/css\" charset=\"utf-8\" media=\"all\" href=\"css/wikistyle.css\">")
;(setq muse-html-header
;  "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n<html>\n  <head>\n    <title><lisp>\n  (concat (muse-publishing-directive \"title\")\n          (let ((author (muse-publishing-directive \"author\")))\n            (if (not (string= author (user-full-name)))\n                (concat \" (by \" author \")\"))))</lisp></title>\n    <meta name=\"generator\" content=\"muse.el\">\n    <meta http-equiv=\"<lisp>muse-html-meta-http-equiv</lisp>\"\n          content=\"<lisp>muse-html-meta-content-type</lisp>\">\n    <lisp>\n      (let ((maintainer (muse-style-element :maintainer)))\n        (when maintainer\n          (concat \"<link rev=\\\"made\\\" href=\\\"\" maintainer \"\\\">\")))\n    </lisp><lisp>\n      (muse-style-element :style-sheet muse-publishing-current-style)\n    </lisp>\n  </head>\n  <body>\n    <h1><lisp>\n  (concat (muse-publishing-directive \"title\")\n          (let ((author (muse-publishing-directive \"author\")))\n            (if (not (string= author (user-full-name)))\n                (concat \" (by \" author \")\"))))</lisp></h1>\n<div id=\"content\">    <!-- Page published by Emacs Muse begins here -->\n")
;(setq muse-html-footer "</div>\n<!-- Page published by Emacs Muse ends here -->\n  </body>\n</html>\n")
;(setq muse-project-alist
;      '(("memo" ("/scp:leadmain:/home/murphy/work/muse/memo/" :default "index")
;;      '(("memo" ("~/.emacs.d/muse/memo/" :default "index")
;         (:base "html" :path "/scp:leadmain:/home/murphy/work/muse/memo/html"))))
;         (:base "html" :path "/home/murphy/automount/leadmain-home/work/muse/memo/html"))))
;;}}}

;;===========================================================================
;;{{{ restructuredText mode
;;===========================================================================
(require 'rst)
;;}}}

;;===========================================================================
;;{{{ yaml mode
;;===========================================================================
;(require 'yaml-mode)
;(add-to-list 'auto-mode-alist '("\\.yaml$" . yaml-mode))
;;}}}

;;===========================================================================
;;{{{ Misc helper functions
;;===========================================================================
;; remove the dos return/new line
(defun remove-trailing-ctrl-m()
  "remove the dos return/new line"
  (interactive)
  (goto-char (point-min))
  ;you can input ctl-m by C-q C-m
  (while (search-forward-regexp "
" nil t)
    (replace-match "" nil t)))
;;}}}

;;===========================================================================
;;{{{ Alias
;;===========================================================================
(defalias 'e 'find-file)
;;}}}

;;===========================================================================
;;{{{ Keys mapping
;;===========================================================================
(global-set-key [f1] 'lgrep)
(global-set-key [(control x) (control g)] 'grep)

(global-set-key [f2] '(lambda()
			(interactive)
			(find-file "~/.emacs.d/txt/outline-memo.org")))

(global-set-key [f3] '(lambda()
			(interactive)
			(dired "e:/Documents/Projects")))
(global-set-key [f4] 'ibuffer)

(global-set-key [f5] 'linum-mode)

(global-set-key [(meta g)] 'goto-line)


(global-set-key [(f8)] '(lambda()
                          (interactive)
                          (isearch-forward "M.Lu")))

(global-set-key [(control f8)] '(lambda()
                                  (interactive)
                                  (isearch-backward "M.Lu")))


(global-set-key [(f10)] '(lambda ()
			   (interactive)
			   (folding-mode)))

(global-set-key [(control f10)] '(lambda ()
                                   (interactive)
                                   (insert (format-time-string "[%Y-%m-%d %H:%M]"))))

;(global-set-key [f11] 'set_chinese)
(global-set-key [f12] '( lambda() (interactive) (insert "`\\\(\\\)`") (forward-char -3)))



;(global-set-key [C-t-s] 'tags-sear)
;(define-key [\C-t s])
;(if(equal (system-name) "GCOM-W2K")
(global-set-key [(control -)] 'set-mark-command)
;(global-set-key [(control =)] 'set-mark-command)
;(global-set-key [?\S- ] 'set-mark-command)

;occur mode shortkey
(global-set-key [(control f12)] 'occur)

;copy whole word
(global-set-key [(control f11)] 'murphytalk-copy-whole-word)

;imenu - conflict with kmacro
(global-set-key [S-mouse-3] 'imenu)


(defun set-buffer-file-eol-type (eol-type)
   "Set the file end-of-line conversion type of the current buffer to
 EOL-TYPE.
 This means that when you save the buffer, line endings will be converted
 according to EOL-TYPE.

 EOL-TYPE is one of three symbols:

   unix (LF)
   dos (CRLF)
   mac (CR)

 This function marks the buffer modified so that the succeeding
 \\[save-buffer]
 surely saves the buffer with EOL-TYPE.  From a program, if you don't want
 to mark the buffer modified, use coding-system-change-eol-conversion
 directly [weikart]."
   (interactive "SEOL type for visited file (unix, dos, or mac): ")
   (setq buffer-file-coding-system (coding-system-change-eol-conversion
                      buffer-file-coding-system eol-type))
   (set-buffer-modified-p t)
   (force-mode-line-update))
(global-set-key "\^Cu" (lambda () (interactive) (set-buffer-file-eol-type 'unix)))
;;}}}

;;===========================================================================
;;{{{ TAG file list
;;===========================================================================
;(setq tags-table-list
;      '(
;        "C:/LM/Projects/SCP-PRO700/Sim/SimPro700_ALL/TEMPWORK"
;        "C:/LM/Projects/SCP-PRO700/SCP-PRO700"
;	"c:/LM/Projects/SCP-2500/Daily/SCP-2500"
;	"c:/LM/Projects/SCP-8500/Daily/SCP-8500"
;	))
;;}}}

;;===========================================================================
;; Murhytalk's .emacs ends here :)
;;===========================================================================
