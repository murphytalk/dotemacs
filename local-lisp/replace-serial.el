(defun replace-serial-number (from-string to-string &optional delimited start end)
  (interactive
   (let ((common
      (query-replace-read-args
       "Replace string"
       nil)))
     (list (nth 0 common) (nth 1 common) (nth 2 common))))
  (perform-replace-serial-number from-string to-string t nil delimited nil nil)
)

(defun perform-replace-serial-number (from-string replacements
                query-flag regexp-flag delimited-flag
            &optional repeat-count map start end)
  (or map (setq map query-replace-map))
  (and query-flag minibuffer-auto-raise
       (raise-frame (window-frame (minibuffer-window))))
  (let ((nocasify (not (and case-fold-search case-replace
                (string-equal from-string
                      (downcase from-string)))))
    (case-fold-search (and case-fold-search
                   (string-equal from-string
                         (downcase from-string))))
    (literal (or (not regexp-flag) (eq regexp-flag 'literal)))
    (search-function (if regexp-flag 're-search-forward 'search-forward))
    (search-string from-string)
    (real-match-data nil)
    (next-replacement nil)
    (noedit nil)
    (keep-going t)
    (stack nil)
    (replace-count 0)
    (nonempty-match nil)

    (limit nil)

    (match-again t)

    (message
     (if query-flag
         (substitute-command-keys
          "Query replacing %s with %s: (\\<query-replace-map>\\[help] for help) "))))

    (when start
      (setq limit (copy-marker (max start end)))
      (goto-char (min start end))
      (deactivate-mark))

    (cond
     ((stringp replacements)
      (setq next-replacement replacements
            replacements     nil))
     ((stringp (car replacements))
      (or repeat-count (setq repeat-count 1))
      (setq replacements (cons 'replace-loop-through-replacements
                               (vector repeat-count repeat-count
                                       replacements replacements)))))

    (if delimited-flag
    (setq search-function 're-search-forward
          search-string (concat "\\b"
                    (if regexp-flag from-string
                      (regexp-quote from-string))
                    "\\b")))
    (when query-replace-lazy-highlight
      (setq isearch-lazy-highlight-last-string nil))

    (push-mark)
    (undo-boundary)
    (unwind-protect
    (while (and keep-going
            (not (or (eobp) (and limit (>= (point) limit))))
            (setq real-match-data
              (if (consp match-again)
                  (progn (goto-char (nth 1 match-again))
                     (replace-match-data t
                      real-match-data
                      match-again))
                (and (or match-again
                     (progn
                       (forward-char 1)
                       (not (or (eobp)
                        (and limit (>= (point) limit))))))
                 (funcall search-function search-string limit t)
                 (replace-match-data t real-match-data)))))
    
      (setq nonempty-match
        (/= (nth 0 real-match-data) (nth 1 real-match-data)))

      (setq match-again
        (and nonempty-match
             (or (not regexp-flag)
             (and (looking-at search-string)
                  (let ((match (match-data)))
                (and (/= (nth 0 match) (nth 1 match))
                     match))))))

      (unless (and query-replace-skip-read-only
               (text-property-not-all
            (nth 0 real-match-data) (nth 1 real-match-data)
            'read-only nil))

        (when replacements
          (set-match-data real-match-data)
          (setq next-replacement
            (funcall (car replacements) (cdr replacements)
                 replace-count)
            noedit nil))
        (if (not query-flag)
        (let ((inhibit-read-only
               query-replace-skip-read-only))
          (unless (or literal noedit)
            (replace-highlight
             (nth 0 real-match-data) (nth 1 real-match-data)
             start end search-string
             (or delimited-flag regexp-flag) case-fold-search))
          (setq noedit
            (replace-match-maybe-edit
             next-replacement nocasify literal
             noedit real-match-data)
            replace-count (1+ replace-count)))
          (undo-boundary)
          (let (done replaced key def)
;insert I.Takaoka start
	(setq cnt (string-to-int next-replacement))
	(setq leplace_length (length next-replacement))
;insert I.Takaoka start
        (while (not done)
          (set-match-data real-match-data)
          (replace-highlight
           (match-beginning 0) (match-end 0)
           start end search-string
           (or delimited-flag regexp-flag) case-fold-search)
;insert I.Takaoka start
	  (if (> leplace_length (length (int-to-string cnt)))
	      (setq new_replace_string
		    (concat 
		     (make-string (- leplace_length (length (int-to-string cnt))) ?0)
		     (int-to-string cnt)))
	    (setq new_replace_string (int-to-string cnt)))
	  (setq next-replacement new_replace_string)
;insert I.Takaoka end
          (let ((message-log-max nil))
            (message message
                             (query-replace-descr from-string)
                             (query-replace-descr next-replacement)))
          (setq key (read-event))
          (set-match-data real-match-data)
          (setq key (vector key))
          (setq def (lookup-key map key))
          (cond ((eq def 'help)
             (with-output-to-temp-buffer "*Help*"
               (princ
                (concat "Query replacing "
                    (if regexp-flag "regexp " "")
                    from-string " with "
                    next-replacement ".\n\n"
                    (substitute-command-keys
                     query-replace-help)))
               (with-current-buffer standard-output
                 (help-mode))))
            ((eq def 'exit)
             (setq keep-going nil)
             (setq done t))
            ((eq def 'backup)
             (if stack
                 (let ((elt (pop stack)))
                   (goto-char (nth 0 elt))
                   (setq replaced (nth 1 elt)
                     real-match-data
                     (replace-match-data
                      t real-match-data
                      (nth 2 elt))))
               (message "No previous match")
               (ding 'no-terminate)
               (sit-for 1)))
            ((eq def 'act)
             (or replaced
                 (setq noedit
                   (replace-match-maybe-edit
                    next-replacement nocasify literal
                    noedit real-match-data)
                   replace-count (1+ replace-count)))
             (setq done t replaced t))
            ((eq def 'act-and-exit)
             (or replaced
                 (setq noedit
                   (replace-match-maybe-edit
                    next-replacement nocasify literal
                    noedit real-match-data)
                   replace-count (1+ replace-count)))
             (setq keep-going nil)
             (setq done t replaced t))
            ((eq def 'act-and-show)
             (if (not replaced)
                 (setq noedit
                   (replace-match-maybe-edit
                    next-replacement nocasify literal
                    noedit real-match-data)
                   replace-count (1+ replace-count)
                   real-match-data (replace-match-data
                            t real-match-data)
                   replaced t)))
            ((eq def 'automatic)
             (or replaced
                 (setq noedit
                   (replace-match-maybe-edit
                    next-replacement nocasify literal
                    noedit real-match-data)
                   replace-count (1+ replace-count)))
             (setq done t query-flag nil replaced t))
            ((eq def 'skip)
             (setq done t))
            ((eq def 'recenter)
             (recenter nil))
            ((eq def 'edit)
             (let ((opos (point-marker)))
               (setq real-match-data (replace-match-data
                          nil real-match-data
                          real-match-data))
               (goto-char (match-beginning 0))
               (save-excursion
                 (save-window-excursion
                   (recursive-edit)))
               (goto-char opos)
               (set-marker opos nil))
             (if (and regexp-flag nonempty-match)
                 (setq match-again (and (looking-at search-string)
                            (match-data)))))
            ((eq def 'edit-replacement)
             (setq real-match-data (replace-match-data
                        nil real-match-data
                        real-match-data)
                   next-replacement
                   (read-string "Edit replacement string: "
                                            next-replacement)
                   noedit nil)
             (if replaced
                 (set-match-data real-match-data)
               (setq noedit
                 (replace-match-maybe-edit
                  next-replacement nocasify literal noedit
                  real-match-data)
                 replaced t))
             (setq done t))

            ((eq def 'delete-and-edit)
             (replace-match "" t t)
             (setq real-match-data (replace-match-data
                        nil real-match-data))
             (replace-dehighlight)
             (save-excursion (recursive-edit))
             (setq replaced t))
            (t
             (setq this-command 'mode-exited)
             (setq keep-going nil)
             (setq unread-command-events
                   (append (listify-key-sequence key)
                       unread-command-events))
             (setq done t)))
          (when query-replace-lazy-highlight
            (if (not (memq def '(skip backup)))
            (setq isearch-lazy-highlight-last-string nil)))
;insert I.Takaoka start
          (setq cnt (+ cnt 1))
	  (if (> leplace_length (length (int-to-string cnt)))
	      (setq new_replace_string
		    (concat 
		     (make-string (- leplace_length (length (int-to-string cnt))) ?0)
		     (int-to-string cnt)))
	    (setq new_replace_string (int-to-string cnt)))
	  (setq next-replacement new_replace_string)
;insert I.Takaoka end
	  )
        (push (list (point) replaced
                (if replaced
                (list
                 (match-beginning 0)
                 (match-end 0)
                 (current-buffer))
                  (match-data t)))
              stack)))))

      (when (and regexp-flag (not match-again) (> replace-count 0))
    (backward-char 1))

      (replace-dehighlight))
    (or unread-command-events
    (message "Replaced %d occurrence%s"
         replace-count
         (if (= replace-count 1) "" "s")))
    (and keep-going stack)))
