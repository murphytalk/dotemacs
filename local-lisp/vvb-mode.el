;;; vvb-mode.el --- a minor mode to display a transient vertical bar at a column

;; Copyright (C) 1999 Petrotechical Open Software Corp. (POSC)

;; Author:     Gongquan Chen <chen@posc.org>
;; Maintainer: chen@posc.org
;; Created:    Jan-25-1999
;; Version:    0.10
;; Keywords:   minor-mode

;; This file may be part of XEmacs.

;; XEmacs is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; XEmacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with XEmacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This package implements a minor mode for XEmacs which, when enabled,
;; will display a transient visual vertical bar at the point if its
;; column is at or past a user-specified one.

;; To use it, just add this line to your .emacs file:  (require 'vvb-mode)

;; There are 2 user variables used to control this minor mode:
;; 1. vvb-column         : the target column to display the vertical bar
;; 2. vvb-right-on-eol-p : a toggle to extend the bar to the end-of-line

;; You can also change the visuals for the vertical bar by customizing this
;; face variable, `vvb-face'. eg:
;; (set-face-foreground vvb-face "your-fg-color-name")
;; (set-face-background vvb-face "your-bg-color-name")

;;; Caveats:

;; This minor-mode was developed and used in XEmacs v21.0b60. It has not 
;; been tested/used in any previous version of XEmacs and font-lock. It
;; will not work for any version of FSF Emacs.

;;; Code:

(defvar vvb-face (make-face 'vvb-face)
  "Face used to hilite the transient visual vertical bar.")
(set-face-background vvb-face "pink")

(defvar vvb-column 72
  "*The column at which the transient vertical bar is visible.")
(make-variable-buffer-local 'vvb-column)

(defvar vvb-visible-p nil
  "t if transient vertical bar is visible. This is a working variable.")
(make-variable-buffer-local 'vvb-visible-p)

(defvar vvb-right-on-eol-p nil
  "*non-nil will make the right side of the transient vertical bar 
extended to the end of line.")

(defvar vvb-mode nil
  "*Non-nil means display the transient vertical bar
when `point' is at column `vvb-column'.")


(defun vvb-mode (&optional arg) 
  "Toggle Visual Vertical Bar mode.
With arg, turn this mode on iff arg is positive.
When this mode is enabled, a transient vertical bar appears 
if `point' is at or past column `vvb-column'."
  (interactive "P")
  (setq vvb-mode
	(if (null arg) (not vvb-mode)
	  (> (prefix-numeric-value arg) 0)))
  (if vvb-mode
      (add-hook 'post-command-hook 'vvb-show)
    (remove-hook 'post-command-hook 'vvb-show)
    (vvb-hide))
)

(defun vvb-hide ()
  "Hide the transient visual vertical bar if any."
  (if vvb-visible-p
      (progn (setq vvb-visible-p nil)
	     (map-extents '(lambda(x a)
			     (delete-extent x))
			  nil nil nil nil nil
			  'vvb)))
)

(defun vvb-show ()
  "Display a transient visual vertical bar at `point'
if its column equals to `vvb-column'. The bar will be
highlighted using `vvb-face'. If `vvb-right-on-eol-p'
is non-nil, then the right side of the bar will be
extended to the end of line."

  ;; show the bar
  (let ((column (current-column))
	(case-fold-search nil)
	)
    (if (>= column vvb-column)
	(if vvb-visible-p
	    nil
	  (let ((start (window-start))
		(end (window-end))
		(cnt 0)
		)
	    (save-excursion
	      (goto-char start)
	      (while (<= (point) end)
		(move-to-column vvb-column)
		(if (<= vvb-column (current-column))
		    (let (b e)
		      (setq b (point))
		      (if vvb-right-on-eol-p
			  (progn (end-of-line)
				 (setq e (point)))
			(setq e (1+ b)))
		      
		      (set-extent-properties (make-extent b e)
					     (list 'face vvb-face
						   'vvb t))
		      (setq cnt (1+ cnt))))
		
		(or (search-forward "\n" nil t)
		    (setq end -1))))
	    
	    (if (> cnt 0)
		(setq vvb-visible-p t))
	    ))

      ;;else - clean up previous act if any
      (vvb-hide))
    )
)

;; attach it to the XEmacs system
(or (assoc 'vvb-mode minor-mode-alist)
    (setq minor-mode-alist (cons '(vvb-mode " VBar") minor-mode-alist)))

(provide 'vvb-mode)

;;; File vvb-mode.el ends here.


